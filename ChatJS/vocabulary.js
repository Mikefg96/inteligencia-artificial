exports.vocabulary = [
    { phrase: 'Hello!', class: 'Greeting' },
    { phrase: 'Bye!', class: 'Farewell' },
    { phrase: 'Play some music!', class: 'Music' },
    { phrase: 'Play a movie!', class: 'Movie' },
    { phrase: 'How is the weather?', class: 'Weather' }
]