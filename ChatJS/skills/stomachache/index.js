function stomachache_res() {
    return { text: 'You may have stomachache, I suggest an appointment with Dr. Cooper' }
}

const intent = () => ({
    keywords: ['My belly hurts', 'My stomach hurts', 'I ate something and I am not feeling good', 'Stomachache'],
    module: 'stomachache'
});

module.exports = {
    get: stomachache_res,
    intent
}