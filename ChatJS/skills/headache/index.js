function headache_res() {
    return { text: ['You may have a headache, you should see Dr. Strange'] }
}

const intent = () => ({
    keywords: ['My head hurts', 'I hit my head really bad', 'I am feeling dizzy', 'Headache'],
    module: 'headache'
});

module.exports = {
    get: headache_res,
    intent
}