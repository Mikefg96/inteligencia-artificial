function symptoms_res() {
    return { text: 'Describe your symptoms' }
}

const intent = () => ({
    keywords: ['I am not feeling well', 'I am feeling sick', 'I do not feel good', 'I am sick'],
    module: 'symptoms'
});

module.exports = {
    get: symptoms_res,
    intent
}