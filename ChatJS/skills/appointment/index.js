function appointment_res() {
    return { text: 'Sure, your appointment is already scheduled' }
}

const intent = () => ({
    keywords: ['Can I meet him tomorrow', 'I need an appointment as soon as possible', 'Can you make an appointment for next week', 'Appointment'],
    module: 'appointment'
});

module.exports = {
    get: appointment_res,
    intent
}