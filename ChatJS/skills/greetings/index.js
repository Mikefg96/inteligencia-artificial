function greetings_res() {
    return { text: 'Hello, how can I help you?' }
}

const intent = () => ({
    keywords: ['Hello', 'Hi'],
    module: 'greetings'
});

module.exports = {
    get: greetings_res,
    intent
}