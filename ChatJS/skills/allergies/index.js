function allergies_res() {
    return { text: 'You may have allergies, Dr. House is your best option' }
}

const intent = () => ({
    keywords: ['I have been sneezing all day', 'I am feeling all itchy', 'My nose wont stop sneezing', 'I have allergies'],
    module: 'allergies'
});

module.exports = {
    get: allergies_res,
    intent
}