function farewell_res() {
    return { text: 'You are welcome, goodbye!' }
}

const intent = () => ({
    keywords: ['Bye!', 'Goodbye', 'That will be all', 'Thank you so much'],
    module: 'farewell'
});

module.exports = {
    get: farewell_res,
    intent
}