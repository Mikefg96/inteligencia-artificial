var app = require('http').createServer(response);
var fs = require('fs');
var io = require('socket.io')(app);
var natural = require('natural');
var tokenizer = new natural.WordTokenizer();
var classifier = new natural.BayesClassifier();
var vocabulary = require('./vocabulary').vocabulary;
const path = require('path');

var skills = {};
loadSkills();
var NaiveBayesModel = trainModel();

app.listen(3000);
console.log("App running...");

function response(req, res) {
    var file = "";
    if (req.url == "/") {
        file = __dirname + '/index.html';
    } else {
        file = __dirname + req.url;
    }

    fs.readFile(file,
        function(err, data) {
            if (err) {
                res.writeHead(404);
                return res.end('Page or file not found');
            }

            res.writeHead(200);
            res.end(data);
        }
    );
}

io.on("connection", function(socket) {
    socket.on("send message", function(sent_msg, callback) {
        // sent_msg = "[ " + getCurrentDate() + " ]: " + sent_msg;

        // var token = tokenizer.tokenize(sent_msg);
        // var stemmer_msg = natural.PorterStemmer.stem(token);
        var skill = NaiveBayesModel.classify(sent_msg);
        sent_msg = skills[skill].get(sent_msg).text;

        io.sockets.emit("update messages", sent_msg);
        callback();
    });
});

function getCurrentDate() {
    var currentDate = new Date();
    var day = (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate();
    var month = ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1);
    var year = currentDate.getFullYear();
    var hour = (currentDate.getHours() < 10 ? '0' : '') + currentDate.getHours();
    var minute = (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes();
    var second = (currentDate.getSeconds() < 10 ? '0' : '') + currentDate.getSeconds();

    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function trainModel() {
    let skillNames = Object.keys(skills);

    skillNames.forEach(function(skillName) {
        let skill = skills[skillName];

        if (skill.intent) {
            const intent_funct = skill.intent;
            const intent = intent_funct();

            intent.keywords.map(keyword => {
                classifier.addDocument(keyword, skill.name);
            });
        }
    });

    classifier.train();
    return classifier;
}

function getDirectories(scrpath) {
    return fs.readdirSync(scrpath).filter(file =>
        fs.statSync(path.join(scrpath, file)).isDirectory());
}

function loadSkills() {
    const skills_dir = __dirname + '/skills';
    const dirs = getDirectories(skills_dir);

    for (let i = 0; i < dirs.length; i++) {
        const dir = dirs[i];
        const skill = require(`./skills/${dir}`);

        skill.name = dir;
        skills[skill.name] = skill;
    }
}