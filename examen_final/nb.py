import pandas as pd, numpy as np
import nltk
import matplotlib.pyplot as plt
from nltk import sent_tokenize
from nltk.stem import PorterStemmer
from nltk.tokenize.toktok import ToktokTokenizer
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB

# Carga del conjunto de datos
amlo_df = pd.read_csv('amlo.csv', index_col=None, na_values=['NA'], encoding='utf-8')
report_df = pd.read_csv('report.csv', index_col=None, na_values=['NA'], encoding='utf-8')

#Visualización de la información
processed_df = amlo_df.drop(['id'], axis=1)
# print(processed_df.head(10))

'''
NLTK
'''

def indentity(arg):
    return arg

# tokens = nltk.word_tokenize(text)
ps = PorterStemmer()
toktok = ToktokTokenizer()
stop_words = set(stopwords.words('spanish'))

tokenize = processed_df.apply(lambda processed_df: nltk.word_tokenize(processed_df['log']), axis=1)
words = tokenize.apply(lambda x: [item for item in x if item not in stop_words])

report_tokenize = report_df.apply(lambda report_df: nltk.word_tokenize(report_df['log']), axis=1)
report_words = report_tokenize.apply(lambda x: [item for item in x if item not in stop_words])

def stem_sentences(sentence):
    tokens = sentence.split()
    stemmed_tokens = [ps.stem(token) for token in tokens]
    return ' '.join(stemmed_tokens)

processed_df['log'] = processed_df['log'].apply(stem_sentences)

report_df['log'] = report_df['log'].apply(stem_sentences)

'''
Dataframe Vectorizing
'''

vectorize = CountVectorizer(lowercase=False, tokenizer=indentity, preprocessor=None)
data_features = vectorize.fit_transform(processed_df['log'])
report_data_features = vectorize.transform(report_df['log'])

# print(data_features)
# print(report_data_features)

'''
Train test split
'''

data_labels = processed_df['positivo_negativo'].values
X_train, X_test, Y_train, Y_test = train_test_split(data_features, data_labels, test_size=0.3)

'''
Using MultinomialNB model
'''

clf = MultinomialNB()
clf.fit(X_train, Y_train)

predictions = clf.predict(report_data_features)
for prediction in predictions:
    print(prediction)

score = clf.score(X_test, Y_test)
print(score)